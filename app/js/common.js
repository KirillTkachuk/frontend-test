$(function() {

 $('.menu a').click(function(){
  $('.menu li').removeClass('active');
 	$(this).parent('li').addClass('active');
 });	

 $("#nav-toggle").click(function(e) {
		e.preventDefault();
    $(this).toggleClass("active");
    $(".menu").slideToggle(200);
  });

	$('input[type="range"]').rangeslider({
		polyfill: false,
		onSlideEnd: function(position, value) {		
			var inputVal = $('input[type="range"]').val();
  		inputVal == 20 ? $('.rangeslider__fill').css({"width":"100%"}) : null;					
		}
	});

	$('select').easyDropDown({
		cutOff: 9,
		wrapperClass: 'dropdown',
	});


	function inputLabel(){
		$('.user-plcsh input').each(function(){
			if ($(this).val().length > 0 && !$(this).val().match(/^\s*$/)) {
				$(this).siblings('label').addClass('active__input');
			}else{
				$(this).siblings('label').removeClass('active__input');
				$(this).val('');
			}
		});
	};
	inputLabel();
	$('.user-plcsh input').on('input', function(){
		inputLabel();
	});

});
